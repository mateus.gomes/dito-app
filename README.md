## Desafio Dito

### Tecnologias utilizadas:
* Python;
* Framework Django;
* Django REST framework;
* Database SQLite3;

### Pontos Positivos:
* Fácil prototipação;
* Python possui estruturas de dados e regular expression que, além de serem fáceis e flexíveis de usar, são performáticas;

### Possíveis melhorias:
* Utilizar Redis para manter em cache as ultimas consultas por um determinado tempo;
* Utilizar um database mais escalável e performáticas como, por exemplo, o postgresql, sqlserver ou um database não relacional;
* Evoluir estrutura do coletor para tornar realmente utilizável em cenário real;

### Como testar?

* Ativar virtualenv:
    * No terminal, executar comando
substituindo a substring {dir} pelo caminho do diretório da pasta dito-app(incluindo ela): 

        ``` source {dir}/env/bin/activate ``` 
    * Mais detalhes sobre vitualenv neste [link](https://virtualenv.pypa.io/en/latest/userguide/)

* Subir server Django:
    * Ainda no terminal, agora dentro do diretório {dir}/dito-app/dito, executar o comando: 
    
        ``` python manage.py runserver ```

        * Mais detalhes sobre django neste [link](https://docs.djangoproject.com/en/2.2/intro/tutorial01/#the-development-server)

### Testando endpoints das API:
* Utilizar ferramenta de sua preferencia, como por exemplo Postman, swagger ou comando curl.


## EndPoints API
* API Coletor de eventos:
    * Lista de eventos: 
        * GET - http://127.0.0.1:8000/colector/events/api/

    * Evento especifico: 
        * GET - http://127.0.0.1:8000/colector/events/api/{id_evento}/
    
    * Criar evento:
        * POST - http://127.0.0.1:8000/colector/events/api/
        * body - tipo application/json:
        ``` 
        {
            "event": "{nome_evento}",
            "timestamp": "{timestamp_evento}"
        }
        ```
    
    * Alterar evento:
        * PUT - http://127.0.0.1:8000/colector/events/api/{id_evento}/

        * body - tipo application/json:
        ``` 
        {
            "event": "{nome_evento}",
            "timestamp": "{novo_timestamp_evento}"
        }
        ```

    * Deletar evento:
        * DELETE - http://127.0.0.1:8000/colector/events/api/{id_evento}/



## API AutoComplete:
* endpoint: http://127.0.0.1:8000/colector/events/api/search_for/{term}
   * Substituir a substring {term} pelo nome do evento desejado.

## API Timeline:
* GET http://127.0.0.1:8000/timeline/