$('.advancedAutoComplete').autoComplete({
    resolver: 'custom',
    events: {
        search: function (qry, callback) {
            // let's do a custom ajax call
            $.ajax(
                'http://127.0.0.1:8000/colector/events/search_for/',
                {
                    data: {'':qry}
                }
            ).done(function (res) {
                callback(res.results)
            });
        }
    }
});