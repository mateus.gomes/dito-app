from django.db import models
from django.urls import reverse

class Event(models.Model):
    event = models.CharField(max_length=16)
    timestamp = models.DateTimeField('timestamp')
    
    def __str__(self):
        return self.event