from rest_framework.routers import DefaultRouter
from django.urls import path
from . import views

router = DefaultRouter()
router.register('', views.EventViewSet)

urlpatterns = router.urls
urlpatterns.append(path('search/<str:term>', views.search_for))
