from rest_framework import serializers
from. import models

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'event', 'timestamp')
        model = models.Event