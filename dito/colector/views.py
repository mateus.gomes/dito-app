import json
from django.http.response import JsonResponse
from rest_framework import generics
from .serializers import EventSerializer
from rest_framework import viewsets
from .models import Event

class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

def search_for(request, term):
    result = list(Event.objects.filter(event__icontains=term))

    events_name = []
    for r in result:
        events_name.append(r.event)

    if events_name:
        return JsonResponse(events_name, safe=False)
    else:
        return JsonResponse({"detail": "Not found."}, safe=False)


