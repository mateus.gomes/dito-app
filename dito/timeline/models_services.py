import json

class Purchase(json.JSONEncoder): 
    def __init__(self, transaction_id, timestamp, store_name, revenue):
        self.timestamp = timestamp
        self.revenue = revenue
        self.transaction_id = transaction_id
        self.store_name = store_name
        self.products = []

    def add_product(self, name, price):
        hasProduct = [product for product in self.products if product.name == name]
        
        if not hasProduct:
            self.products.append(Product(name, price))

    def default(self, o):
        return json.JSONEncoder(self, o)

    def __eq__(self, other):
        return self.transaction_id == other.transaction_id

    def __str__(self):
        return self.transaction_id


class Product(json.JSONEncoder): 
    def __init__(self, name, price):
        self.name = name
        self.price = price
    
    def default(self, o):
        return json.JSONEncoder(self, o)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name