import json
import requests
from .models_services import Purchase, Product

class EventServices:

    def __init__(self, url = 'http://storage.googleapis.com/dito-questions/events.json'):
        self.url = url
        self.purchase = []
        self.__obj_response = []
        self.__partial_obj_by_type = []
        self.__partial_obj_by_item = []


    def __get_events(self):
        response = requests.get(self.url)
        response.raise_for_status()

        events_dict = response.json()
        events_list = events_dict['events']
        
        self.__obj_response = events_list

    def __split_response_by_type(self):
        self.__partial_obj_by_type = [i for i in self.__obj_response if i['event'] == 'comprou']
        for item in self.__partial_obj_by_type:
            custom_data = item['custom_data']
            item['custom_data'] = sorted(custom_data, key=lambda i: i['key'])

    def __split_response_by_itens(self):
        self.__partial_obj_by_item = [event for event in self.__obj_response if event['event'] == 'comprou-produto']
        # order custom data by transaction_id
        for item in self.__partial_obj_by_item:
            custom_data = item['custom_data']
            item['custom_data'] = sorted(custom_data, key=lambda i: i['key'])


    def __join_partials_objs(self):
        r = []

        for p in self.__partial_obj_by_type:
            for i in self.__partial_obj_by_item:
                transaction_id  = p['custom_data'][1]['value']
                transaction_id_product = i['custom_data'][2]['value']
                timestamp = p['timestamp']
                store_name = p['custom_data'][0]['value']
                revenue = p['revenue']
                product_name = i['custom_data'][0]['value']
                price = i['custom_data'][1]['value']
                
                if transaction_id == transaction_id_product:
                    purchase = [s for s in r if s.transaction_id == transaction_id]

                    if not purchase:
                        purchase = Purchase(transaction_id, timestamp, store_name, revenue)
                        purchase.add_product(product_name, price)

                        r.append(purchase)
                    else:
                        purchase[0].add_product(product_name, price)

        self.purchase = r

    def __order_by_asc_timestamp(self):
        pass
        #self.purchase = sorted(self.purchase, key=lambda p: p['timestamp'], reverse=True)

    def get_purchases(self):
        self.__get_events()
        self.__split_response_by_type()
        self.__split_response_by_itens()
        self.__join_partials_objs()
        self.__order_by_asc_timestamp()

        return self.purchase 
