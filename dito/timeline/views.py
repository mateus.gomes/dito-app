from django.http import HttpResponse
import json
from .services import EventServices


def index(request):
    purchases = EventServices().get_purchases()
    data = {'timeline' : purchases}
    
    teste = json.dumps(data, default=lambda o: o.__dict__)

    return HttpResponse(teste, content_type='application/json')

